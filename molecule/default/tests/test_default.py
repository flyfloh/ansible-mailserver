import os

import testinfra.utils.ansible_runner
import pytest

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.mark.parametrize("name, port, is_open", [
    ("smtp", 25, True),
    ("smtp", 465, False),
    ("smtp", 587, True),
    ("imap", 143, True),
    ("imaps", 993, False),
    ("pop", 110, False),
    ("pops", 995, False),
])
def test_open_ports(host, name, port, is_open):
    port = host.socket("tcp://{}".format(port))
    assert port.is_listening == is_open


# Something is wrong on the Ubuntu image with the postfix service. It does not
# register as running, even though ps aux shows it to be
@pytest.mark.parametrize("name", "running", "enabled", [
    ("postfix", False, True),
    ("dovecot", True, True),
])
def test_services(host, name, running, enabled):
    service = host.service(name)
    assert service.is_running == running
    assert service.is_enabled == enabled
