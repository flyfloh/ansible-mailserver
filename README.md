# Mailserver

An Ansible Playbook to deploy a Mailserver with Postfix and Dovecot.

## Variables

Set the `mail_fqdn_list` to include all domains where your server should be
reachable.

```yaml
mail_fqdn_list:
  - mail.example.com
  - mail.foo.bar
```

Set `alias_domain_list` to all include all domains you want to handle mail for.

```yaml
alias_domain_list:
  - example.com
  - foo.bar
```

Set `virtual_mail` to include an `address` and a `user` for all mail addresses
you wish to handle.

```yaml
virtual_mail:
   - { address: foo@example.com, user: alice }
   - { address: bar@example.com, user: bob }
   - { address: foo@foo.bar, user: alice }
```
